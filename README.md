# SLS — Yet Another Image Viewer

## Aim

With SLS, I aim to create an image viewer that tries to find the middle between having one big list of images and images neatly organised into folders and subfolders. It assumes images are organised in folders, but displays them in a way that's more like (but not identical to) a long list.

A screen shot may help make this a bit clearer:

![Overview](SLS-overview.png)

The images in the current folder are displayed as thumbnails. Clicking on an image opens an image carousel with all the images in the current folder:

![Carousel](SLS-carousel.png)

Subfolders are displayed using a folder image, with a thumbnail of the first image in the subfolder if it holds images. Clicking on the folder (or on the folder label, i.e., the grey bar showing the folder's path) expands it. Here, for example, the Screenshots folder is expanded:

![Folder Expanded](SLS-folder_expanded.png)

Note that the expanded folder does not replace the main image folder. Rather, the images and subfolders are added to the list.

Clicking on the folder label collapses the folder again.

## Status

This is very much still Works For Me™. The basics work, but if the image library is a bit larger, loading is unacceptably slow.
