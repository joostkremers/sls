# Circular List class adapted from https://stackoverflow.com/questions/4151320/efficient-circular-buffer

from typing import TypeVar, Generic

T = TypeVar("T")


class CircularList(Generic[T]):
    """A circular list class.

    A circular list is a list whose index is never out of range. Say `mylist`
    has four elements, `mylist[4]` refers to the first element in the list,
    i.e., it is the same element referred to by `mylist[0]`.

    A circular list has a fixed size. Elements can be added to the end
    (`append`) or to the beginning (`prepend`) of the list. If the list has
    reached its maximum length, adding an element pushes out the element on the
    opposite end of the list. Note that prepending an element to the list is not
    more costly than appending one, because in the underlying data structure,
    both methods of adding an element to the list reduce to appending or
    replacing an element.

    Attributes
    ==========
    size:
        The maximum length of the circular list.

    """

    _start: int
    _data: list[T]
    size: int

    def __init__(self, size: int, data: list[T] = []):
        """Initialize a circular list.

        Create a circular list with the given size and optional data. If the
        data contains more elements than `size` allows, it is truncated.

        Parameters
        ----------
        size
            The maximum length of the circular list.
        data
            The initial data.

        Examples
        --------
        c = CircularList(4)
        c.append(1); print(c)
        ==> [1] (1/4 items)

        d = CircularList(4, [1, 2, 3, 4, 5]); print(d)
        ==> [2, 3, 4, 5] (4/4 items) # list is truncated

        """
        if size <= 0:
            raise ValueError(f"Cannot create circular list of size {size}.")
        self.size = size
        self._data = list(data)[-size:]
        # We set the start of the list to `len(self._data)`, i.e., one position
        # *after* the last element. Given that the list is circular, this
        # corresponds to position 0, but doing it this way ensures that `_start`
        # points to the position where new elements are added (cf. methods
        # `append` and `prepend`), even if the list is not full.
        self._start = len(self._data)

    def __getitem__(self, index: int) -> T:
        """Return the element at position `index`.

        The first element of the list is at index position 0. `index` can be any
        integer. If it falls outside the range of the list, it is reduced using
        the modulo operation. Negative indices are allowed and count backwards
        from the end of the list.

        Parameters
        ----------
        index
            The index of the element to be retrieved.

        """
        return self._data[(index + self._start) % len(self._data)]

    def __str__(self):
        """Return a string representation of a CircularList."""
        return (
            f"{self._data[self._start :] + self._data[: self._start]!r}"
            + f" ({len(self._data)!r}"
            + f"/{self.size!r} items)"
        )

    def __repr__(self):
        """Return the canonical string representation of a CircularList."""
        return f"CircularList({self.size!r}, {self._data[self._start :] + self._data[: self._start]!r})"

    def __len__(self):
        return len(self._data)

    def append(self, value: T):
        """Append an element to a CircularList.

        The element is added to the end of the list. If the list has reached its
        maximum size, the first element of the list is dropped.

        Parameters
        ----------
        value
            The value to add.

        """
        if len(self._data) == self.size:
            self._data[self._start % self.size] = value
        else:
            self._data.append(value)
        self._start = (self._start + 1) % self.size

    def prepend(self, value: T):
        """Prepend an element to a CircularList.

        The element is added to the beginning of the list. If the list has
        reached it maximum size, the last element of the list is dropped.

        Parameters
        ----------
        value
            The value to add.

        """
        if len(self._data) == self.size:
            self._data[self._start - 1] = value
            self._start = (self._start - 1) % self.size
        else:
            self._data.append(value)
            self._start = len(self._data) - 1
