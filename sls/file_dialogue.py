import os

from kivy.properties import StringProperty, ListProperty

from kivy.uix.modalview import ModalView


class FileDialogue(ModalView):
    selection: StringProperty = StringProperty("")
    filters: ListProperty = ListProperty()

    def __init__(self, path: str, *args, **kwargs):
        self.rootpath = path
        self.filters = [FileDialogue.filter_file]
        super(FileDialogue, self).__init__(*args, **kwargs)
        self.selection = path

    def toggle_hidden(self):
        self.ids["file_chooser"].show_hidden = not self.ids["file_chooser"].show_hidden
        self.ids["file_chooser"]._update_files()

    @staticmethod
    def filter_file(_folder: str, file: str) -> bool:
        # Note that the `file` argument is passed an absolute file name, so we
        # don't need the `folder` argument at all.
        if os.path.isdir(file):
            return True
        else:
            return False
