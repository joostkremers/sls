from kivy.core.window import Window

from kivy.properties import ObjectProperty

from kivy.uix.modalview import ModalView
from kivy.uix.carousel import Carousel

from sls.rotatable_image import RotatableImage


class ImageGallery(ModalView):
    gallery: Carousel = ObjectProperty()

    def __init__(self, images: list[str], first: int, **kwargs):
        super(ImageGallery, self).__init__(**kwargs)
        for image in images:
            widget = RotatableImage(source=image)
            self.gallery.add_widget(widget)
            self.gallery.index = first
        Window.bind(on_key_down=self.key_action)

    def key_action(self, _win, keycode, _codepoint, _text, _modifiers):
        if keycode == 275:  # cursor right
            self.gallery.load_next()
        elif keycode == 276:  # cursor left
            self.gallery.load_previous()

    def on_dismiss(self):
        Window.unbind(on_key_down=self.key_action)
