import os.path
from typing import Any

from kivy.properties import ObjectProperty, ListProperty, NumericProperty

from kivy.metrics import dp

from kivy.uix.label import Label
from kivy.uix.modalview import StringProperty
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.behaviors import ButtonBehavior
from kivy.uix.image import Image

from sls.sparsegridlayout import SparseGridLayout, SparseGridEntry
from sls.image_library import ImageLibrary
from sls.utils import chunk, prettify_path


class SLSImage(ButtonBehavior, SparseGridEntry, Image):
    pass


class SLSFolder(ButtonBehavior, SparseGridEntry, Image):
    pass


class SLSFolderLabel(RecycleDataViewBehavior, ButtonBehavior, Label):
    index: int = 0

    def refresh_view_attrs(self, rv, index, data):
        self.index = index
        super(SLSFolderLabel, self).refresh_view_attrs(rv, index, data)


class ImagePanelItem(RecycleDataViewBehavior, SparseGridLayout):
    image_paths = ListProperty()
    path: StringProperty = StringProperty()
    index: int = 0
    folder: bool = False

    def refresh_view_attrs(self, rv, index, data):
        self.index = index
        self.type = data["type"]
        super(ImagePanelItem, self).refresh_view_attrs(rv, index, data)

    def on_image_paths(self, _instance, _value):
        self.clear_widgets()
        for index, path in enumerate(self.image_paths):
            if self.type == "folder":
                image = SLSFolder(row=0, column=0, source=path)
            else:
                image = SLSImage(row=0, column=index, source=path)
            self.add_widget(image)


class ImagePanel(RecycleView):
    # This is the RecycleView that holds the images. Each item in the ImagePanel
    # is either an SLSFolderLabel, or an ImagePanelItem. An ImagePanelItem holds
    # a single row of images, or a single folder image. Note that we do not put
    # all the images of a single folder into one ImagePanelItem.

    library: ImageLibrary = ObjectProperty()
    columns: NumericProperty = NumericProperty(3)
    distance_to_top: NumericProperty = NumericProperty()
    scrollable_distance: NumericProperty = NumericProperty()

    def __init__(self, *args, **kwargs):
        super(ImagePanel, self).__init__(*args, **kwargs)
        # We put an empty label at the top of the RecycleView, otherwise the
        # first image row is too close to the menu bar.
        label = {
            "widget": "Label",
            "text": "",
            "height": dp(10),
            "expanded": False,
            # These two are needed when the window is resized (cf. on_columns() method).
            "type": "label",
            "path": ".",
        }
        self.data.append(label)

    # The following two methods are copied verbatim from the example
    # `infinite_scrolling.py`. Their purpose is to maintain the RecycleView's
    # position when elements are added or removed from it. Note that if scroll_y
    # is 0, we're scrolled to the bottom of the RecycleView, in which case
    # scroll_y is not updated. This makes sense in the infinite scrolling
    # example, I'm not sure it makes sense here. But it doesn't seem to harm,
    # either.

    def on_scrollable_distance(self, *_args):
        """Adjust the `scroll_y` property to maintain position in the RecycleView."""
        if self.scroll_y > 0:
            self.scroll_y = (
                self.scrollable_distance - self.distance_to_top
            ) / self.scrollable_distance

    def on_scroll_y(self, *_args):
        """Save the distance_to_top every time the RecycleView is scrolled."""
        self.distance_to_top = (1 - self.scroll_y) * self.scrollable_distance

    def create_image_row(self, images: list[str]) -> dict[str, Any]:
        """Create a row of images.

        The returned dict should be added to the data attribute of the ImagePanel.

        Parameters
        ----------
        images
            List of image paths.
        cols
            Number of images per row.

        Returns
        -------
        dict
            Data representing the image row.

        """

        thumbnails = [self.library.create_thumbnail(file) for file in images]
        return {
            "widget": "ImagePanelItem",
            "columns": self.columns,
            "rows": 1,
            "image_paths": thumbnails,
            "type": "images",
        }

    def create_folder_row(self, path: str) -> dict[str, Any]:
        """Create a folder row.

        A folder row consists of a single image (the thumbnail of the first
        image in the directory) inside a folder icon. The returned dict should be
        added to the data attribute of the ImagePanel.

        Parameters
        ----------
        path
            Path of the directory to display in the folder row.

        Returns
        -------
        dict
            Data representing the folder row.

        """
        image_path = self.library.first_image(path)
        thumbnail = self.library.create_thumbnail(image_path)
        return {
            "widget": "ImagePanelItem",
            "columns": self.columns,
            "rows": 1,
            "image_paths": [thumbnail],
            "type": "folder",
            "path": path,
        }

    def create_label(self, path: str) -> dict[str, Any]:
        label = {
            "widget": "SLSFolderLabel",
            "text": prettify_path(path),
            "path": path,
            "type": "label",
            "height": dp(20),
            "expanded": False,
        }
        return label

    def images_to_data(self, directory: str, files: list[str]) -> list[dict[str, Any]]:
        """Create a list of data dicts for `files`."""
        images: list[dict[str, Any]] = []
        rows = chunk(files, self.columns)
        for row in rows:
            new_row = self.create_image_row(
                [os.path.join(directory, file) for file in row]
            )
            images.append(new_row)
        return images

    def add_folder(
        self,
        directory: str,
        replace: int | None = None,
    ) -> None:
        """Add the contents of a directory to the SLSView.

        This creates a series of ImagePanelItem objects to represent the images
        and the subdirectories of `directory`. In addition, each subdirectory is
        preceded by an SLSFolderLabel. If `replace` is given, it should be the
        index of an ImagePanelItem that displays a folder, which is going to be
        expanded.

        Parameters
        ----------
        directory
            Path of the directory to be added, relative to `self.library.folder.root`.
        replace
            Index of the item that needs to be replaced.

        """

        subdirs, files = self.library.contents[directory]

        # If the directory argument is ".", we change it to the empty string,
        # because the directory is combined with the names of its subdirs using
        # `os.path.join`. With '.', this leads to paths like "./subdir", which
        # would give ugly folder labels " . › subdir".

        if directory == ".":
            directory = ""

        new_data: list[dict[str, Any]]

        if files:
            new_data = self.images_to_data(directory, files)
        else:
            new_data = []

        for subdir in subdirs:
            subdir_path = os.path.join(directory, subdir)
            label = self.create_label(subdir_path)
            new_data.append(label)
            new_row = self.create_folder_row(subdir_path)
            new_data.append(new_row)

        if replace is not None:
            self.data[replace : replace + 1] = new_data
            self.data[replace - 1]["expanded"] = True
        else:
            self.data.extend(new_data)

    def collapse_folder(self, folder: SLSFolderLabel):
        i = folder.index
        self.data[i]["expanded"] = False
        path = self.data[i]["path"]
        # The first item that contains data under `folder`:
        start = i + 1
        while i < len(self.data) and (
            self.data[i]["type"] != "label" or self.data[i]["path"].startswith(path)
        ):
            i += 1
        folder_row = self.create_folder_row(path)
        self.data[start:i] = [folder_row]

    def on_columns(self, _instance, _value):
        # We go through the `self.data` list backwards, so we don't have to keep
        # adjusting the index variable `i`. Note that the while loop runs as
        # long as `i` is larger than 0, because we decrement `i` before we use
        # it to access `self.data`.
        i: int = len(self.data)
        while i > 0:
            i -= 1
            if self.data[i]["type"] == "label":
                continue
            elif self.data[i]["type"] == "folder":
                path = self.data[i - 1]["path"]
                self.data[i] = self.create_folder_row(path)
            else:
                # Find all the image rows and replace them with recalculated image rows.
                last = i
                while self.data[i]["type"] == "images":
                    i -= 1
                path = self.data[i]["path"]
                files = self.library.contents[path][1]
                self.data[i + 1 : last + 1] = self.images_to_data(path, files)
