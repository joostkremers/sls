from PIL import Image as PILImage

from kivy.properties import NumericProperty

from kivy.uix.image import Image

# We check the EXIF tag "Orientation" to see if we need to rotate an image. To
# apply the rotation, we check the `source` attribute.
#
# For EXIF tags and their values, see here:
# <https://exiftool.org/TagNames/EXIF.html>.
#
# These are the values the Orientation tag can have:
#
# 1 = Horizontal (normal)
# 2 = Mirror horizontal
# 3 = Rotate 180
# 4 = Mirror vertical
# 5 = Mirror horizontal and rotate 270 CW
# 6 = Rotate 90 CW
# 7 = Mirror horizontal and rotate 90 CW
# 8 = Rotate 270 CW
#
# Currently, we only implement 1, 3, 6 and 8. Note that while EXIF rotations are
# clockwise, Kivy rotates counter-clockwise.

# ROTATIONS maps EXIF orientation values to Kivy rotation angles. RESIZABLE
# lists those orientations where a resizing of the image may be necessary.
ROTATIONS = {1: 0, 3: 180, 6: 270, 8: 90}
RESIZABLE = [6, 8]


class RotatableImage(Image):
    rotation: NumericProperty = NumericProperty()

    def on_source(self, _instance, source: str):
        exif_data: PILImage.Exif | None = None
        with PILImage.open(source) as pil_image:
            exif_data = pil_image.getexif()
        if exif_data:
            orientation = exif_data.get(274, 1)
            self.rotation = ROTATIONS[orientation]
            if orientation in RESIZABLE:
                self.size_hint_y = 1 / self.image_ratio
