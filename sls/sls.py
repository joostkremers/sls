import os.path
from typing import Any

from kivy.app import App

from kivy.core.window import Window

from kivy.properties import ObjectProperty

from kivy.uix.boxlayout import BoxLayout

from sls.file_dialogue import FileDialogue
from sls.image_library import ImageLibrary
from sls.image_panel import ImagePanel, SLSFolder, SLSFolderLabel
from sls.gallery import ImageGallery
from sls.utils import prettify_path


class SLSView(BoxLayout):
    """The main window of the program.

    The SLSView contains a MenuBar and a RecycleView that holds the actual
    images.

    Attributes
    ----------
    library
        The ImageLibrary instance holding the image data.
    panel
        The ImagePanel instance displaying the images.

    """

    library: ImageLibrary = ObjectProperty()
    panel: ImagePanel = ObjectProperty()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        Window.bind(on_resize=self.resize_window)

    def open_library(self):
        self.file_opener = FileDialogue(os.path.expanduser("~"))
        self.file_opener.open()

    # These functions are bound to user events in the kv file. They are
    # collected here, even though they are bound to widgets not defined here.

    def load_library(self, path: str):
        self.file_opener.dismiss()
        self.library = ImageLibrary(path)
        root = os.path.relpath(path, os.path.expanduser("~"))
        self.ids["menubar"].text = prettify_path(root)

        self.panel.add_folder(".")

    def show_carousel(self, thumbnail_path: str):
        """Open the image carousel.

        The image carousel is opened with the image corresponding to the
        thumbnail as the first image. The other images in the directory
        containing the thumbnail are part of the carousel.

        Parameters
        ----------
        thumbnail_path
            Path to the thumbnail for which the image should be shown.

        """
        img_dir = self.library.thumbnail_to_dir(thumbnail_path)
        img_name = os.path.basename(thumbnail_path)
        img_names = self.library.contents[img_dir][1]
        index = img_names.index(img_name)
        img_paths = [os.path.join(self.library.root, img_dir, img) for img in img_names]
        image_carousel = ImageGallery(img_paths, index)
        image_carousel.open()

    def expand_folder(self, folder: SLSFolder):
        """Show a folder's contents.

        This function is called when a folder is clicked. It displays the
        folder's contents.

        Parameters
        ----------
        folder
            Folder to expand.

        """
        self.panel.add_folder(folder.path, replace=folder.index)

    def toggle_folder(self, label: SLSFolderLabel):
        """Expand or collapse a folder.

        This function is called when a folder label is clicked. It expands or
        collapses the folder.

        Parameters
        ----------
        label
            Label of the folder to be expanded or collapsed.

        """

        datum: dict[str, Any] = self.panel.data[label.index]
        # The first widget in the ImagePanel is a label, but it's there just for
        # padding, so it cannot be clicked.
        if datum["path"] == ".":
            return
        if datum["expanded"]:
            self.panel.collapse_folder(label)
        else:
            self.panel.add_folder(datum["path"], replace=label.index + 1)

    def resize_window(self, _window, width, _height):
        # When the main window is resized, We simply set the new number of image
        # columns. The `on_columns` callback handles the rest. Note that this
        # callback function is only called when the value of `columns` actually
        # changes. If it's assigned a new value that's equal to the old value,
        # the callback is not called. Therefore we can safely assign the new
        # value unconditionally:
        self.panel.columns = width // 250


class SLSApp(App):
    def build(self):
        return SLSView()


if __name__ == "__main__":
    SLSApp().run()
