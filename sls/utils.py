import os
from typing import Iterator, TypeVar

T = TypeVar("T")


def chunk(lst: list[T], chunk_size: int) -> Iterator[list[T]]:
    for i in range(0, len(lst), chunk_size):
        yield lst[i : i + chunk_size]


def prettify_path(path: str) -> str:
    return path.replace(os.path.sep, " › ")


def get_file_extension(file: str) -> str:
    return os.path.splitext(file)[1][1:].lower()
