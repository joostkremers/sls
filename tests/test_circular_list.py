import pytest

from sls.circular_list import CircularList


class TestCircularList:
    @pytest.mark.parametrize(
        "data,length", [([], 0), ([1, 2, 3, 4], 4), ([1, 2, 3, 4, 5], 5)]
    )
    def test_len(self, data: list[int], length: int):
        l = CircularList(5, data=data)
        assert len(l) == length

    def test_append1(self):
        l = CircularList(5, [1, 2, 3])
        l.append(4)
        assert len(l) == 4

    def test_append2(self):
        l = CircularList(5, [1, 2, 3, 4])
        l.append(5)
        assert len(l) == 5

    def test_append3(self):
        l = CircularList(5, [1, 2, 3, 4, 5])
        l.append(6)
        assert len(l) == 5
